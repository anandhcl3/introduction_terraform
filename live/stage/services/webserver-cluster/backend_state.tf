#configuring s3 backend
terraform {
backend "s3" {
   bucket = "anandlavu2018"
   key    = "terraform/web_cluster/stage/terraform.tfstate"
   region = "ap-south-1"
  }
}
